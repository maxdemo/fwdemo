# DATA FILE
$FILENAME = "fw.txt"

if (-not (Test-Path $FILENAME)) {
    throw "File ``$FILENAME`` not found!"
}

$data = Get-Content $FILENAME
$ln = 0
$v4 = 0
$v6 = 0
foreach ($line in $data) {
    $ln += 1

    if($line.StartsWith('#') -or [string]::IsNullOrEmpty($line)) {
        continue
    }

    try {
        $ip = [ipaddress] $line
        if ($ip.AddressFamily.ToString().Contains("V6")) {
            $v6 += 1
        }
        else {
            $v4 += 1
        }
    }
    catch {
        throw "An exception occured in file $($FILENAME):$ln ``$line``: $($_)"
    }
}

Write-Host "Successfully checked all $ln adresses. Found $v4 IPv4 and $v6 IPv6 adresses."